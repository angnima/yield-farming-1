// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract FarmContract {
    
    struct StakeDetail {
        address userAddress;
        bool    isStaking;
        uint256 startTime;
        uint256 stakingBalance;
        uint256 myTokenBalance;
    }

    string public name = "FarmContract";

    IERC20 public maticToken;
    IERC20 public myToken;

    mapping(address => StakeDetail) public stakes;

    event Stake(address indexed from, uint256 amount);
    event Unstake(address indexed from, uint256 amount);
    event YieldWithdrawn(address indexed to, uint256 amount);

    constructor(IERC20 _maticToken, IERC20 _myToken) {
        maticToken = _maticToken;
        myToken = _myToken;
    }

    function stakeToken(uint256 amount) public {
        require(amount > 0 && maticToken.balanceOf(msg.sender) >= amount, "Token Balance not enough!");
        require(maticToken.allowance(msg.sender, address(this)) >= amount, "Token Allowance not enough!");

        StakeDetail stake = stakes[msg.sender];
        
        if(stake.isStaking == true){
            uint256 toTransfer = calculateTotalYield(msg.sender);
            stake.myTokenBalance += toTransfer;
        }

        maticToken.transferFrom(msg.sender, address(this), amount);

        totalStakedBalance += amount;
        
        stake.stakingBalance += amount;
        stake.startTime = block.timestamp;
        stake.isStaking = true;

        emit Stake(msg.sender, amount);
    }

    function unstake(uint256 amount) public {
        StakeDetail stake = stakes[msg.sender];
        require(stake.isStaking = true && stake.stakingBalance >= amount, "Nothing to unstake");

        uint256 yieldTransfer = calculateTotalYield(msg.sender);
        stake.startTime = block.timestamp; // bug fix
        uint256 balanceTransfer = amount;
        amount = 0;
        stake.stakingBalance -= balanceTransfer;
        
        maticToken.transfer(msg.sender, balanceTransfer);
        totalStakedBalance -= amount;
        stake.myTokenBalance += yieldTransfer;
        
        if(stake.stakingBalance == 0){
            stake.isStaking = false;
        }

        emit Unstake(msg.sender, amount);
    }
    
    function withdrawYield() public {
        uint256 toTransfer = calculateTotalYield(msg.sender);
        StakeDetail stake = stakes[msg.sender];

        require(toTransfer > 0 || stake.myTokenBalance > 0, "Nothing to withdraw");
            
        if(stake.myTokenBalance != 0){
            uint256 oldBalance = stake.myTokenBalance;
            stake.myTokenBalance = 0;
            toTransfer += oldBalance;
        }

        stake.startTime = block.timestamp;

        myToken.transfer(msg.sender, toTransfer);

        emit YieldWithdrawn(msg.sender, toTransfer);
    }

    function calculateYieldTime(address user) public view returns(uint256){
        uint256 end = block.timestamp;
        uint256 totalTime = end - startTime[user];
        return totalTime;
    }

    function calculateTotalYield(address user) public view returns(uint256) {
        uint256 time = calculateYieldTime(user) * 10**18;
        uint256 rate = 86400;
        uint256 timeRate = time / rate;
        uint256 rawYield = (stakingBalance[user] * timeRate) / 10**18;
        return rawYield;
    } 

}